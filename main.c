//
// Created by vargat on 2019. 11. 13.
//
#include <stdio.h>
#include <stdlib.h>
#include "util.h"
#include "email.h"

int process(const char *email) {
    validity_code errorCode;
    char *errorMessage;

    if (!email) {
#ifdef DEBUG
        printf("Null pointer!");
#endif
        return EXIT_FAILURE;
    }

    if ((errorCode = email_checking(email)) == E_NULL_POINTER) {
#ifdef DEBUG
        printf("Null pointer!");
#endif
        return EXIT_FAILURE;
    } else {
        errorMessage = validity_string(errorCode);

        if (!errorMessage)
            return EXIT_FAILURE;

        printf(errorCode == 0 ? ANSI_COLOR_GREEN : ANSI_COLOR_RED);
        printf("Checking: '%s' ... %s\n"ANSI_COLOR_RESET, email, errorMessage);
        free(errorMessage);
        return EXIT_SUCCESS;
    }
}

int main(int argc, char **argv) {
    if (argc != 2) {
        usage(*argv);
        return EXIT_FAILURE;
    }
    return process(*(argv + 1));
}


// hello