//
// Created by vargat on 2019. 11. 25.
//

#ifndef EMAIL_CHECK_C_EMAIL_H
#define EMAIL_CHECK_C_EMAIL_H

#define LOCAL_PART_MAX_LENGTH 64
#define DOMAIN_PART_MAX_LENGTH 64
#define MESSAGE_MAX_LENGTH 128

typedef enum {
    EMAIL_VALID = 0,
    E_FIRST_IS_AT,
    E_FIRST_IS_POINT,
    E_LAST_IS_AT,
    E_LAST_IS_POINT,
    E_TOO_MANY_AT,
    E_NO_AT,
    E_NO_POINT_IN_DOMAIN,
    E_POINT_AT_TOGETHER,
    E_CONTAINS_NON_PRINTABLE_CHAR,
    E_LOCAL_PART_TOO_LONG,
    E_DOMAIN_PART_TOO_LONG,
    E_ILLEGAL_CHARACTER_IN_DOMAIN,
    E_HYPHEN_FIRST_IN_DOMAIN,
    E_LAST_IS_HYPHEN,
    E_NULL_POINTER

} validity_code;

validity_code email_checking(const char *);

char *validity_string(validity_code);

#endif //EMAIL_CHECK_C_EMAIL_H
