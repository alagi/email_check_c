//
// Created by vargat on 2019. 11. 25.
//
#include <stdio.h>
#include <stdlib.h>
#include "email.h"

validity_code email_checking(const char *email) {
    int i = 0;
    int atPosition = 0;
    int pointCount = 0;
    char currentChar;
    char previousChar = '\0';

    if (!email) {
        return E_NULL_POINTER;
    }

    while ((currentChar = *(email + i++)) != '\0') {

        switch (currentChar) {
            case 1 ... 32:
            case 127:
                return E_CONTAINS_NON_PRINTABLE_CHAR;
                /* Non printable characters are not allowed */

            case '@':
                if (i - 1 == 0)
                    return E_FIRST_IS_AT;

                if (atPosition > 0)
                    return E_TOO_MANY_AT;

                if (i - 1 > LOCAL_PART_MAX_LENGTH)
                    return E_LOCAL_PART_TOO_LONG;

                if (previousChar == '.')
                    return E_POINT_AT_TOGETHER;

                atPosition = i - 1;
                break;

            case '.':
                if (atPosition > 0)
                    pointCount++;

                if (previousChar == '@' || previousChar == '.')
                    return E_POINT_AT_TOGETHER;

                if (i - 1 == 0)
                    return E_FIRST_IS_POINT;

                if (atPosition > 0 && previousChar == '-')
                    return E_HYPHEN_FIRST_IN_DOMAIN;
                break;

            case '-':
                if (atPosition > 0 && (previousChar == '@' || previousChar == '.'))
                    return E_HYPHEN_FIRST_IN_DOMAIN;
                break;

            default:;
        }  // end of switch

        if (atPosition > 0 && currentChar != '@') {
            if ((currentChar >= '0' && currentChar <= '9') || currentChar == '.' || currentChar == '-' ||
                (currentChar >= 'A' && currentChar <= 'Z') || (currentChar >= 'a' && currentChar <= 'z')) {
            } else
                return E_ILLEGAL_CHARACTER_IN_DOMAIN;
        }
        previousChar = currentChar;
    } // end of while

    if (atPosition == 0)
        return E_NO_AT;

    if (!pointCount)
        return E_NO_POINT_IN_DOMAIN;

    if (previousChar == '.')
        return E_LAST_IS_POINT;

    if (previousChar == '@')
        return E_LAST_IS_AT;

    if (previousChar == '-')
        return E_LAST_IS_HYPHEN;

    if (i - 1 - atPosition > DOMAIN_PART_MAX_LENGTH)
        return E_DOMAIN_PART_TOO_LONG;

    return EMAIL_VALID;

}

char *validity_string(validity_code code) {
    char *message = malloc(MESSAGE_MAX_LENGTH);

    if (!message)
        return NULL;

    switch (code) {
        case EMAIL_VALID:
            sprintf(message, "Email is valid");
            break;
        case E_FIRST_IS_AT:
            sprintf(message, "Not valid: First character is @");
            break;
        case E_FIRST_IS_POINT:
            sprintf(message, "Not valid: First character is .");
            break;
        case E_LAST_IS_AT:
            sprintf(message, "Not valid: Last character is @");
            break;
        case E_LAST_IS_POINT:
            sprintf(message, "Not valid: Last character is .");
            break;
        case E_LAST_IS_HYPHEN:
            sprintf(message, "Not valid: Last character is -");
            break;
        case E_TOO_MANY_AT:
            sprintf(message, "Not valid: Contains more than 1 @");
            break;
        case E_NO_AT:
            sprintf(message, "Not valid: @ is missing");
            break;
        case E_NO_POINT_IN_DOMAIN:
            sprintf(message, "Not valid: . is missing in domain part");
            break;
        case E_POINT_AT_TOGETHER:
            sprintf(message, "Not valid: '@.' or '..' or '.@' together is not allowed");
            break;
        case E_CONTAINS_NON_PRINTABLE_CHAR:
            sprintf(message, "Not valid: Contains non printable character");
            break;
        case E_LOCAL_PART_TOO_LONG:
            sprintf(message, "Not valid: Local part before @ is longer than %d characters", LOCAL_PART_MAX_LENGTH);
            break;
        case E_DOMAIN_PART_TOO_LONG:
            sprintf(message, "Not valid: Domain part after @ is longer than %d characters", DOMAIN_PART_MAX_LENGTH);
            break;
        case E_NULL_POINTER:
            sprintf(message, "Not valid: Null pointer");
            break;
        case E_ILLEGAL_CHARACTER_IN_DOMAIN:
            sprintf(message, "Not valid: Domain part contains illegal character, only [a..zA..Z0..9.-] is allowed");
            break;
        case E_HYPHEN_FIRST_IN_DOMAIN:
            sprintf(message, "Not valid: Hyphen only allowed between alphabet characters in domain part");
            break;
        default:
            sprintf(message, "Unknown error");
    }
    return message;
    // It is necessary to free by the caller!!!
}